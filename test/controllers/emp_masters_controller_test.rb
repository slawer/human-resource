require 'test_helper'

class EmpMastersControllerTest < ActionController::TestCase
  setup do
    @emp_master = emp_masters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emp_masters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emp_master" do
    assert_difference('EmpMaster.count') do
      post :create, emp_master: { address: @emp_master.address, dob: @emp_master.dob, email: @emp_master.email, gender: @emp_master.gender, lastname: @emp_master.lastname, marital_status: @emp_master.marital_status, number: @emp_master.number, othernames: @emp_master.othernames, photo: @emp_master.photo, religion: @emp_master.religion, role_id: @emp_master.role_id }
    end

    assert_redirected_to emp_master_path(assigns(:emp_master))
  end

  test "should show emp_master" do
    get :show, id: @emp_master
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emp_master
    assert_response :success
  end

  test "should update emp_master" do
    patch :update, id: @emp_master, emp_master: { address: @emp_master.address, dob: @emp_master.dob, email: @emp_master.email, gender: @emp_master.gender, lastname: @emp_master.lastname, marital_status: @emp_master.marital_status, number: @emp_master.number, othernames: @emp_master.othernames, photo: @emp_master.photo, religion: @emp_master.religion, role_id: @emp_master.role_id }
    assert_redirected_to emp_master_path(assigns(:emp_master))
  end

  test "should destroy emp_master" do
    assert_difference('EmpMaster.count', -1) do
      delete :destroy, id: @emp_master
    end

    assert_redirected_to emp_masters_path
  end
end
