require 'test_helper'

class EmpRequestsControllerTest < ActionController::TestCase
  setup do
    @emp_request = emp_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emp_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emp_request" do
    assert_difference('EmpRequest.count') do
      post :create, emp_request: { emp_leave_master_id: @emp_request.emp_leave_master_id, request_date: @emp_request.request_date, return_date: @emp_request.return_date, return_status: @emp_request.return_status, status: @emp_request.status }
    end

    assert_redirected_to emp_request_path(assigns(:emp_request))
  end

  test "should show emp_request" do
    get :show, id: @emp_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emp_request
    assert_response :success
  end

  test "should update emp_request" do
    patch :update, id: @emp_request, emp_request: { emp_leave_master_id: @emp_request.emp_leave_master_id, request_date: @emp_request.request_date, return_date: @emp_request.return_date, return_status: @emp_request.return_status, status: @emp_request.status }
    assert_redirected_to emp_request_path(assigns(:emp_request))
  end

  test "should destroy emp_request" do
    assert_difference('EmpRequest.count', -1) do
      delete :destroy, id: @emp_request
    end

    assert_redirected_to emp_requests_path
  end
end
