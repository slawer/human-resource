require 'test_helper'

class EmpLeavesControllerTest < ActionController::TestCase
  setup do
    @emp_leafe = emp_leaves(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emp_leaves)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emp_leafe" do
    assert_difference('EmpLeave.count') do
      post :create, emp_leafe: { emp_id: @emp_leafe.emp_id, end_date: @emp_leafe.end_date, leave_id: @emp_leafe.leave_id, reported_date: @emp_leafe.reported_date, start_date: @emp_leafe.start_date, status: @emp_leafe.status }
    end

    assert_redirected_to emp_leafe_path(assigns(:emp_leafe))
  end

  test "should show emp_leafe" do
    get :show, id: @emp_leafe
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emp_leafe
    assert_response :success
  end

  test "should update emp_leafe" do
    patch :update, id: @emp_leafe, emp_leafe: { emp_id: @emp_leafe.emp_id, end_date: @emp_leafe.end_date, leave_id: @emp_leafe.leave_id, reported_date: @emp_leafe.reported_date, start_date: @emp_leafe.start_date, status: @emp_leafe.status }
    assert_redirected_to emp_leafe_path(assigns(:emp_leafe))
  end

  test "should destroy emp_leafe" do
    assert_difference('EmpLeave.count', -1) do
      delete :destroy, id: @emp_leafe
    end

    assert_redirected_to emp_leaves_path
  end
end
