require 'test_helper'

class RolesMastersControllerTest < ActionController::TestCase
  setup do
    @roles_master = roles_masters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:roles_masters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create roles_master" do
    assert_difference('RolesMaster.count') do
      post :create, roles_master: { description: @roles_master.description, role_name: @roles_master.role_name, status: @roles_master.status }
    end

    assert_redirected_to roles_master_path(assigns(:roles_master))
  end

  test "should show roles_master" do
    get :show, id: @roles_master
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @roles_master
    assert_response :success
  end

  test "should update roles_master" do
    patch :update, id: @roles_master, roles_master: { description: @roles_master.description, role_name: @roles_master.role_name, status: @roles_master.status }
    assert_redirected_to roles_master_path(assigns(:roles_master))
  end

  test "should destroy roles_master" do
    assert_difference('RolesMaster.count', -1) do
      delete :destroy, id: @roles_master
    end

    assert_redirected_to roles_masters_path
  end
end
