require 'test_helper'

class HealthMastersControllerTest < ActionController::TestCase
  setup do
    @health_master = health_masters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:health_masters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create health_master" do
    assert_difference('HealthMaster.count') do
      post :create, health_master: { question: @health_master.question }
    end

    assert_redirected_to health_master_path(assigns(:health_master))
  end

  test "should show health_master" do
    get :show, id: @health_master
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @health_master
    assert_response :success
  end

  test "should update health_master" do
    patch :update, id: @health_master, health_master: { question: @health_master.question }
    assert_redirected_to health_master_path(assigns(:health_master))
  end

  test "should destroy health_master" do
    assert_difference('HealthMaster.count', -1) do
      delete :destroy, id: @health_master
    end

    assert_redirected_to health_masters_path
  end
end
