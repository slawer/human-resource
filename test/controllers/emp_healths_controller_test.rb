require 'test_helper'

class EmpHealthsControllerTest < ActionController::TestCase
  setup do
    @emp_health = emp_healths(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emp_healths)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emp_health" do
    assert_difference('EmpHealth.count') do
      post :create, emp_health: { answer: @emp_health.answer, emp_id: @emp_health.emp_id, health_master_id: @emp_health.health_master_id }
    end

    assert_redirected_to emp_health_path(assigns(:emp_health))
  end

  test "should show emp_health" do
    get :show, id: @emp_health
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emp_health
    assert_response :success
  end

  test "should update emp_health" do
    patch :update, id: @emp_health, emp_health: { answer: @emp_health.answer, emp_id: @emp_health.emp_id, health_master_id: @emp_health.health_master_id }
    assert_redirected_to emp_health_path(assigns(:emp_health))
  end

  test "should destroy emp_health" do
    assert_difference('EmpHealth.count', -1) do
      delete :destroy, id: @emp_health
    end

    assert_redirected_to emp_healths_path
  end
end
