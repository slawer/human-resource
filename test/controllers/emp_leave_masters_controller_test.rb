require 'test_helper'

class EmpLeaveMastersControllerTest < ActionController::TestCase
  setup do
    @emp_leave_master = emp_leave_masters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emp_leave_masters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emp_leave_master" do
    assert_difference('EmpLeaveMaster.count') do
      post :create, emp_leave_master: { emp_id: @emp_leave_master.emp_id, leave_id: @emp_leave_master.leave_id, status: @emp_leave_master.status, user_id: @emp_leave_master.user_id }
    end

    assert_redirected_to emp_leave_master_path(assigns(:emp_leave_master))
  end

  test "should show emp_leave_master" do
    get :show, id: @emp_leave_master
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emp_leave_master
    assert_response :success
  end

  test "should update emp_leave_master" do
    patch :update, id: @emp_leave_master, emp_leave_master: { emp_id: @emp_leave_master.emp_id, leave_id: @emp_leave_master.leave_id, status: @emp_leave_master.status, user_id: @emp_leave_master.user_id }
    assert_redirected_to emp_leave_master_path(assigns(:emp_leave_master))
  end

  test "should destroy emp_leave_master" do
    assert_difference('EmpLeaveMaster.count', -1) do
      delete :destroy, id: @emp_leave_master
    end

    assert_redirected_to emp_leave_masters_path
  end
end
