class CreateEmpHealths < ActiveRecord::Migration
  def change
    create_table :emp_healths do |t|
      t.integer :health_master_id
      t.string :answer
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
