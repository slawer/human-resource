class CreateEmpLeaves < ActiveRecord::Migration
  def change
    create_table :emp_leaves do |t|
      t.integer :emp_id
      t.integer :leave_id
      t.boolean :status
      t.date :start_date
      t.date :end_date
      t.date :reported_date

      t.timestamps null: false
    end
  end
end
