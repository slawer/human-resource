class CreateEmpRequests < ActiveRecord::Migration
  def change
    create_table :emp_requests do |t|
      t.integer :emp_leave_master_id
      t.date :request_date
      t.boolean :status
      t.date :return_date
      t.boolean :return_status

      t.timestamps null: false
    end
  end
end
