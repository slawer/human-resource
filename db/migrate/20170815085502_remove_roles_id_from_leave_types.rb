class RemoveRolesIdFromLeaveTypes < ActiveRecord::Migration
  def change
    remove_column :leave_types, :roles_id, :integer
  end
end
