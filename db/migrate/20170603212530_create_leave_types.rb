class CreateLeaveTypes < ActiveRecord::Migration
  def change
    create_table :leave_types do |t|
      t.string :leave_name
      t.integer :num_days
      t.string :gender
      t.integer :roles_id
      t.boolean :status
      t.boolean :status2

      t.timestamps null: false
    end
  end
end
