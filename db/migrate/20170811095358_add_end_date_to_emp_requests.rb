class AddEndDateToEmpRequests < ActiveRecord::Migration
  def change
    add_column :emp_requests, :end_date, :date
  end
end
