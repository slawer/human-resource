class CreateEmpMasters < ActiveRecord::Migration
  def change
    create_table :emp_masters do |t|
      t.string :lastname
      t.string :othernames
      t.string :gender
      t.date :dob
      t.string :marital_status
      t.string :religion
      t.string :email
      t.integer :number
      t.string :address
      t.integer :role_id
      t.string :photo

      t.timestamps null: false
    end
  end
end
