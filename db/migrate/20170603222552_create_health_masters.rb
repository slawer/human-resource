class CreateHealthMasters < ActiveRecord::Migration
  def change
    create_table :health_masters do |t|
      t.string :question

      t.timestamps null: false
    end
  end
end
