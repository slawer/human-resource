class CreateEmpLeaveMasters < ActiveRecord::Migration
  def change
    create_table :emp_leave_masters do |t|
      t.integer :emp_id
      t.integer :leave_id
      t.boolean :status
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
