class AddUserIdToEmpRequests < ActiveRecord::Migration
  def change
    add_column :emp_requests, :user_id, :integer
  end
end
