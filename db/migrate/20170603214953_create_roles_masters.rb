class CreateRolesMasters < ActiveRecord::Migration
  def change
    create_table :roles_masters do |t|
      t.string :role_name
      t.string :description
      t.boolean :status

      t.timestamps null: false
    end
  end
end
