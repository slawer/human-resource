# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170815085502) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "emp_healths", force: :cascade do |t|
    t.integer  "health_master_id"
    t.string   "answer"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "user_id"
  end

  create_table "emp_leave_masters", force: :cascade do |t|
    t.integer  "emp_id"
    t.integer  "leave_id"
    t.boolean  "status"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emp_leaves", force: :cascade do |t|
    t.integer  "emp_id"
    t.integer  "leave_id"
    t.boolean  "status"
    t.date     "start_date"
    t.date     "end_date"
    t.date     "reported_date"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "emp_masters", force: :cascade do |t|
    t.string   "lastname"
    t.string   "othernames"
    t.string   "gender"
    t.date     "dob"
    t.string   "marital_status"
    t.string   "religion"
    t.string   "email"
    t.integer  "number"
    t.string   "address"
    t.integer  "role_id"
    t.string   "photo"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "user_id"
  end

  create_table "emp_requests", force: :cascade do |t|
    t.integer  "emp_leave_master_id"
    t.date     "request_date"
    t.boolean  "status"
    t.date     "return_date"
    t.boolean  "return_status"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "user_id"
    t.date     "end_date"
  end

  create_table "health_masters", force: :cascade do |t|
    t.string   "question"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "leave_types", force: :cascade do |t|
    t.string   "leave_name"
    t.integer  "num_days"
    t.string   "gender"
    t.boolean  "status"
    t.boolean  "status2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles_masters", force: :cascade do |t|
    t.string   "role_name"
    t.string   "description"
    t.boolean  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "username"
    t.integer  "role_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
