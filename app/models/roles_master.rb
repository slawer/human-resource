class RolesMaster < ActiveRecord::Base
 validates :role_name, presence: {message: "Role name cannot be blank"}
 validates :description, presence: {message: "Description cannot be blank"}
end
