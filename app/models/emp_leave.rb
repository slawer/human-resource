class EmpLeave < ActiveRecord::Base
	belongs_to :leave_type, foreign_key: :leave_id, class_name: "LeaveType"
	belongs_to :emp_master, foreign_key: :emp_id, class_name: "EmpMaster"
	
end
