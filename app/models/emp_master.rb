class EmpMaster < ActiveRecord::Base
	belongs_to :roles_master, foreign_key: :role_id, class_name: "RolesMaster"

	has_many :emp_leaves, foreign_key: :emp_id, class_name: "EmpLeave"
end
