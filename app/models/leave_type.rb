class LeaveType < ActiveRecord::Base
	validates :leave_name, presence: {message: "Leave name cannot be blank"}
  validates :num_days, presence: {message: "Number days cannot be blank"}
  validates :gender, presence: {message: "Gender cannot be blank"}
  
end
