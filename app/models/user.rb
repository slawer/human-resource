class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

belongs_to :roles_masters, foreign_key: :role_id, class_name: "RolesMaster"

  validates :username, presence: {message: "Username cannot be blank"}
  validates :email, presence: {message: "Email cannot be blank"}
  validates :password, presence: {message: "Password cannot be blank"}
  validates :password_confirmation, presence: {message: "Password confirmation cannot be blank"}
  validates :role_id, presence: {message: "Role ID cannot be blank"}
 



  def admin?
    self.role_id == 3
  end

  def user?
    self.role_id == 4
  end

   def employee?
      EmpMaster.where(user_id: self.id)[0]
  end
end
