json.extract! leave_type, :id, :leave_id, :leave_name, :num_days, :gender, :roles_id, :status, :status2, :created_at, :updated_at
json.url leave_type_url(leave_type, format: :json)
