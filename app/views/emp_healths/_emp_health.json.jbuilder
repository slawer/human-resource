json.extract! emp_health, :id, :emp_id, :health_master_id, :answer, :created_at, :updated_at
json.url emp_health_url(emp_health, format: :json)
