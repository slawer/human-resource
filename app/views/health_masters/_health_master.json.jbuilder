json.extract! health_master, :id, :question, :created_at, :updated_at
json.url health_master_url(health_master, format: :json)
