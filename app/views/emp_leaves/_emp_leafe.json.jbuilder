json.extract! emp_leafe, :id, :emp_id, :leave_id, :status, :start_date, :end_date, :reported_date, :created_at, :updated_at
json.url emp_leafe_url(emp_leafe, format: :json)
