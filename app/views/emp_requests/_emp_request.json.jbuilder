json.extract! emp_request, :id, :emp_leave_master_id, :request_date, :status, :return_date, :return_status, :created_at, :updated_at
json.url emp_request_url(emp_request, format: :json)
