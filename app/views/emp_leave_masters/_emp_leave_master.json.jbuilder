json.extract! emp_leave_master, :id, :emp_id, :leave_id, :status, :user_id, :created_at, :updated_at
json.url emp_leave_master_url(emp_leave_master, format: :json)
