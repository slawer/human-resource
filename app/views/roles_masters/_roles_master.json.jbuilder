json.extract! roles_master, :id, :role_name, :description, :status, :created_at, :updated_at
json.url roles_master_url(roles_master, format: :json)
