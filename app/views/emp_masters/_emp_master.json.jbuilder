json.extract! emp_master, :id, :lastname, :othernames, :gender, :dob, :marital_status, :religion, :email, :number, :address, :role_id, :photo, :created_at, :updated_at
json.url emp_master_url(emp_master, format: :json)
