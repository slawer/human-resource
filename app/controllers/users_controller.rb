class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
load_and_authorize_resource
 
  # GET /users
  # GET /users.json
  def index
    @user = User.new
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
    @users = RolesMaster.all
  end

  # GET /users/1/edit
  def edit
    @users = RolesMaster.all
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @users = RolesMaster.all

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'Users was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @users = RolesMaster.all
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'Users was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Users was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
     params.require(:user).permit(:username, :email, :password, :password_confirmation,:user_type_id , :entity_type_id,:entity_master_id,:status,:user_id ,:creator_id, :role_id)
    end
end