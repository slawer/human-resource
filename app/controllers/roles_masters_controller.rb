class RolesMastersController < ApplicationController
  before_action :set_roles_master, only: [:show, :edit, :update, :destroy]
load_and_authorize_resource
 
  # GET /roles_masters
  # GET /roles_masters.json
  def index
    @roles_masters = RolesMaster.all
  end

  # GET /roles_masters/1
  # GET /roles_masters/1.json
  def show
  end

  # GET /roles_masters/new
  def new
    @roles_master = RolesMaster.new
  end

  # GET /roles_masters/1/edit
  def edit
  end

  # POST /roles_masters
  # POST /roles_masters.json
  def create
    @roles_master = RolesMaster.new(roles_master_params)

    respond_to do |format|
      if @roles_master.save
        format.html { redirect_to @roles_master, notice: 'Roles master was successfully created.' }
        format.json { render :show, status: :created, location: @roles_master }
      else
        format.html { render :new }
        format.json { render json: @roles_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /roles_masters/1
  # PATCH/PUT /roles_masters/1.json
  def update
    respond_to do |format|
      if @roles_master.update(roles_master_params)
        format.html { redirect_to @roles_master, notice: 'Roles master was successfully updated.' }
        format.json { render :show, status: :ok, location: @roles_master }
      else
        format.html { render :edit }
        format.json { render json: @roles_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roles_masters/1
  # DELETE /roles_masters/1.json
  def destroy
    @roles_master.destroy
    respond_to do |format|
      format.html { redirect_to roles_masters_url, notice: 'Roles master was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_roles_master
      @roles_master = RolesMaster.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def roles_master_params
      params.require(:roles_master).permit(:role_name, :description, :status)
    end
end
