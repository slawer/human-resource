class EmpRequestsController < ApplicationController
  before_action :set_emp_request, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
 
  # GET /emp_requests
  # GET /emp_requests.json

  def index
    # @emp_requests = EmpRequest.where("user_id = ? or ?", "#{current_user.id}, #{current_user.creator_id}")
    if current_user.role_id == 3
      @emp_requests = EmpRequest.all
    elsif current_user.role_id == 4 && current_user.id
      @emp_requests = EmpRequest.where(user_id: current_user.id)
    end

    employee = EmpMaster.where(user_id: current_user.id).last
    if employee
      @emp_leaves=EmpLeaveMaster.joins(:leave_type).select("leave_name, leave_types.id").where(emp_id: employee.id)
    else

    end
  end

  # GET /emp_requests/1
  # GET /emp_requests/1.json
  def show
    employee = EmpMaster.where(user_id: current_user.id).last
    if employee
      @emp_leaves=EmpLeaveMaster.joins(:leave_type).select("leave_name, leave_types.id").where(emp_id: employee.id)
    else

    end
  end

  # GET /emp_requests/new
  def new
    @emp_request = EmpRequest.new
    employee = EmpMaster.where(user_id: current_user.id).last
    if employee
      @emp_leaves=EmpLeaveMaster.joins(:leave_type).select("leave_name, leave_types.id").where(emp_id: employee.id)
    else

    end
     #@emp_leaves=EmpLeave.all
    logger.info "this is emp leaves: #{@emp_leaves.inspect}"
    logger.info "------------------------------------------"
    
    logger.info "------------------------------------------"
    logger.info "------------------------------------------"
    logger.info "------------------------------------------"
  end

  # GET /emp_requests/1/edit
  def edit
    employee = EmpMaster.where(user_id: current_user.id).last
    if employee
      @emp_leaves=EmpLeaveMaster.joins(:leave_type).select("leave_name, leave_types.id").where(emp_id: employee.id)
    else

    end
    #@emp_leaves=EmpLeaveMaster.joins(:leave_type).where(emp_id: current_user.id)
  end

  # POST /emp_requests
  # POST /emp_requests.json
  def create
    @emp_request = EmpRequest.new(emp_request_params)

    employee = EmpMaster.where(user_id: current_user.id).last
    if employee
      emp_leaves = EmpLeaveMaster.joins(:leave_type).select("num_days, leave_types.id").where(emp_id: employee.id).last
    

    start = @emp_request.request_date
    leave_days = emp_leaves.num_days
    puts "Leave days #{leave_days}"
    end_date = EmpRequest.cal_leave(start,leave_days)

    respond_to do |format|
      if @emp_request.save
        @emp_request.end_date = end_date
        @emp_request.save
        format.html { redirect_to @emp_request, notice: 'Emp request was successfully created.' }
        format.json { render :show, status: :created, location: @emp_request }
      else
        format.html { render :new }
        format.json { render json: @emp_request.errors, status: :unprocessable_entity }
      end
    end
  end
  end
  # PATCH/PUT /emp_requests/1
  # PATCH/PUT /emp_requests/1.json
  def update
    employee = EmpMaster.where(user_id: current_user.id).last
    if employee
      @emp_leaves=EmpLeaveMaster.joins(:leave_type).select("leave_name, leave_types.id").where(emp_id: employee.id)
    else

    end
    respond_to do |format|
      if @emp_request.update(emp_request_params)
        format.html { redirect_to @emp_request, notice: 'Emp request was successfully updated.' }
        format.json { render :show, status: :ok, location: @emp_request }
      else
        format.html { render :edit }
        format.json { render json: @emp_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emp_requests/1
  # DELETE /emp_requests/1.json
  def destroy
    @emp_request.destroy
    respond_to do |format|
      format.html { redirect_to emp_requests_url, notice: 'Emp request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emp_request
      @emp_request = EmpRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def emp_request_params
      params.require(:emp_request).permit(:emp_leave_master_id, :request_date, :status, :return_date, :return_status, :end_date)
    end
end
