class EmpMastersController < ApplicationController
  before_action :set_emp_master, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
 
  # GET /emp_masters
  # GET /emp_masters.json
  def index
    @emp_masters = EmpMaster.all
  end

  # GET /emp_masters/1
  # GET /emp_masters/1.json
  def show
  end

  # GET /emp_masters/new
  def new
    @emp_master = EmpMaster.new
  end

  # GET /emp_masters/1/edit
  def edit
  end

  # POST /emp_masters
  # POST /emp_masters.json
  def create
    @emp_master = EmpMaster.new(emp_master_params)

    respond_to do |format|
      if @emp_master.save
        format.html { redirect_to @emp_master, notice: 'Emp master was successfully created.' }
        format.json { render :show, status: :created, location: @emp_master }
      else
        format.html { render :new }
      end
        format.json { render json: @emp_master.errors, status: :unprocessable_entity }
    end
  end

  # PATCH/PUT /emp_masters/1
  # PATCH/PUT /emp_masters/1.json
  def update
    respond_to do |format|
      if @emp_master.update(emp_master_params)
        format.html { redirect_to @emp_master, notice: 'Emp master was successfully updated.' }
        format.json { render :show, status: :ok, location: @emp_master }
      else
        format.html { render :edit }
        format.json { render json: @emp_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emp_masters/1
  # DELETE /emp_masters/1.json
  def destroy
    @emp_master.destroy
    respond_to do |format|
      format.html { redirect_to emp_masters_url, notice: 'Emp master was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emp_master
      @emp_master = EmpMaster.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def emp_master_params
      params.require(:emp_master).permit(:user_id, :lastname, :othernames, :gender, :dob, :marital_status, :religion, :email, :number, :address, :role_id, :photo)
    end
end
