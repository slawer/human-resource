class HealthMastersController < ApplicationController
  before_action :set_health_master, only: [:show, :edit, :update, :destroy]
load_and_authorize_resource
 
  # GET /health_masters
  # GET /health_masters.json
  def index
    @health_master = HealthMaster.new
    @health_masters = HealthMaster.all
  end

  # GET /health_masters/1
  # GET /health_masters/1.json
  def show
  end

  # GET /health_masters/new
  def new
    @health_master = HealthMaster.new
  end

  # GET /health_masters/1/edit
  def edit
  end

  # POST /health_masters
  # POST /health_masters.json
  def create
    @health_master = HealthMaster.new(health_master_params)

    respond_to do |format|
      if @health_master.save
        format.html { redirect_to @health_master, notice: 'Health master was successfully created.' }
        format.json { render :show, status: :created, location: @health_master }
      else
        format.html { render :new }
        format.json { render json: @health_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /health_masters/1
  # PATCH/PUT /health_masters/1.json
  def update
    respond_to do |format|
      if @health_master.update(health_master_params)
        format.html { redirect_to @health_master, notice: 'Health master was successfully updated.' }
        format.json { render :show, status: :ok, location: @health_master }
      else
        format.html { render :edit }
        format.json { render json: @health_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /health_masters/1
  # DELETE /health_masters/1.json
  def destroy
    @health_master.destroy
    respond_to do |format|
      format.html { redirect_to health_masters_url, notice: 'Health master was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_health_master
      @health_master = HealthMaster.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def health_master_params
      params.require(:health_master).permit(:question)
    end
end
