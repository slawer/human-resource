class EmpLeavesController < ApplicationController
  before_action :set_emp_leave, only: [:show, :edit, :update, :destroy]
load_and_authorize_resource
 
  # GET /emp_leaves
  # GET /emp_leaves.json
  def index
    @emp_leaves = EmpLeave.all
  end

  # GET /emp_leaves/1
  # GET /emp_leaves/1.json
  def show
  end

  # GET /emp_leaves/new
  def new
    @emp_leave = EmpLeave.new

    #loads all the employee data onto the page
    @emp_masters = EmpMaster.all

    @leave_types= LeaveType.all

  end

  # GET /emp_leaves/1/edit
  def edit
    @emp_masters = EmpMaster.all
    @leave_types= LeaveType.all
  end

  # POST /emp_leaves
  # POST /emp_leaves.json
  def create
    @emp_leave = EmpLeave.new(emp_leave_params)

    respond_to do |format|
      if @emp_leave.save
        format.html { redirect_to @emp_leave, notice: 'Emp leave was successfully created.' }
        format.json { render :show, status: :created, location: @emp_leave }
      else
        format.html { render :new }
        format.json { render json: @emp_leave.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emp_leaves/1
  # PATCH/PUT /emp_leaves/1.json
  def update
    respond_to do |format|
      if @emp_leave.update(emp_leave_params)
        format.html { redirect_to @emp_leave, notice: 'Emp leave was successfully updated.' }
        format.json { render :show, status: :ok, location: @emp_leafe }
      else
        format.html { render :edit }
        format.json { render json: @emp_leave.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emp_leaves/1
  # DELETE /emp_leaves/1.json
  def destroy
    @emp_leave.destroy
    respond_to do |format|
      format.html { redirect_to emp_leaves_url, notice: 'Emp leave was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emp_leave
      @emp_leave = EmpLeave.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def emp_leave_params
      params.require(:emp_leave).permit(:emp_id, :leave_id, :status, :start_date, :end_date, :reported_date)
    end
end
