class EmpHealthsController < ApplicationController
  before_action :set_emp_health, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  
  # GET /emp_healths
  # GET /emp_healths.json
  def index
    @emp_healths = EmpHealth.all
  end

  # GET /emp_healths/1
  # GET /emp_healths/1.json
  def show
  end

  # GET /emp_healths/new
  def new
    @emp_health = EmpHealth.new
  end

  # GET /emp_healths/1/edit
  def edit
  end

  # POST /emp_healths
  # POST /emp_healths.json
  def create
    @emp_health = EmpHealth.new(emp_health_params)

    respond_to do |format|
      if @emp_health.save
        format.html { redirect_to @emp_health, notice: 'Emp health was successfully created.' }
        format.json { render :show, status: :created, location: @emp_health }
      else
        format.html { render :new }
        format.json { render json: @emp_health.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emp_healths/1
  # PATCH/PUT /emp_healths/1.json
  def update
    respond_to do |format|
      if @emp_health.update(emp_health_params)
        format.html { redirect_to @emp_health, notice: 'Emp health was successfully updated.' }
        format.json { render :show, status: :ok, location: @emp_health }
      else
        format.html { render :edit }
        format.json { render json: @emp_health.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emp_healths/1
  # DELETE /emp_healths/1.json
  def destroy
    @emp_health.destroy
    respond_to do |format|
      format.html { redirect_to emp_healths_url, notice: 'Emp health was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emp_health
      @emp_health = EmpHealth.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def emp_health_params
      params.require(:emp_health).permit(:emp_id, :health_master_id, :answer, :user_id)
    end
end
