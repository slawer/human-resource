# class EmpLeaveMastersController < InheritedResources::Base

#   private

#     def emp_leave_master_params
#       params.require(:emp_leave_master).permit(:emp_id, :leave_id, :status, :user_id)
#     end
# end



class EmpLeaveMastersController < ApplicationController
  before_action :set_emp_leave_master, only: [:show, :edit, :update, :destroy]
load_and_authorize_resource
 
  # GET /emp_leaves
  # GET /emp_leaves.json
  def index
    @emp_leave_masters = EmpLeaveMaster.all
  end

  # GET /emp_leaves/1
  # GET /emp_leaves/1.json
  def show
  end

  # GET /emp_leaves/new
  def new
    @emp_leave_master = EmpLeaveMaster.new

    #loads all the employee data onto the page
    @emp_masters = EmpMaster.all

    @leave_types= LeaveType.all

  end

  # GET /emp_leaves/1/edit
  def edit
    @emp_masters = EmpMaster.all
    @leave_types= LeaveType.all
  end

  # POST /emp_leaves
  # POST /emp_leaves.json
  def create
    @emp_leave_master = EmpLeaveMaster.new(emp_leave_master_params)

    respond_to do |format|
      if @emp_leave_master.save
        format.html { redirect_to @emp_leave_master, notice: 'Emp leave was successfully created.' }
        format.json { render :show, status: :created, location: @emp_leave_master }
      else
        format.html { render :new }
        format.json { render json: @emp_leave_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emp_leaves/1
  # PATCH/PUT /emp_leaves/1.json
  def update
    respond_to do |format|
      if @emp_leave_master.update(emp_leave_master_params)
        format.html { redirect_to @emp_leave_master, notice: 'Emp leave was successfully updated.' }
        format.json { render :show, status: :ok, location: @emp_leave_master }
      else
        format.html { render :edit }
        format.json { render json: @emp_leave_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emp_leaves/1
  # DELETE /emp_leaves/1.json
  def destroy
    @emp_leave_master.destroy
    respond_to do |format|
      format.html { redirect_to emp_leave_masters_url, notice: 'Emp leave was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emp_leave_master
      @emp_leave_master = EmpLeaveMaster.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def emp_leave_master_params
       params.require(:emp_leave_master).permit(:emp_id, :leave_id, :status, :user_id)
    end
end










